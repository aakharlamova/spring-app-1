package ru.kharlamova.mySpringApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kharlamova.mySpringApp.model.Movie;
import ru.kharlamova.mySpringApp.service.MovieService;
import java.util.List;

@RestController
@RequestMapping("movies/")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Movie> getMovie(@PathVariable("id") Integer movieId) {
        if(movieId == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Movie movie = this.movieService.getById(movieId);

        if(movie == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(movie, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Movie> saveMovie(@RequestBody Movie movie) {
        HttpHeaders headers = new HttpHeaders();

        if(movie == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        this.movieService.save(movie);
        return new ResponseEntity<>(movie, headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Movie> updateMovie(Movie movie, UriComponentsBuilder builder) {
        HttpHeaders headers = new HttpHeaders();
        Movie movieToUpdate = movieService.getById(movie.getId());
        this.movieService.update(movieToUpdate, movie);
        return new ResponseEntity<>(movie, headers, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Movie> deleteMovie(@PathVariable("id") Integer movieId) {
        Movie movie = this.movieService.getById(movieId);

        if(movie == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        this.movieService.delete(movie);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.ALL_VALUE)
    public ResponseEntity<List<Movie>> getAllMovie() {
        List<Movie> movies = movieService.getAll();
        if(movies.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(movies, HttpStatus.OK);
        }
    }

}
