package ru.kharlamova.mySpringApp.service;
import ru.kharlamova.mySpringApp.model.Movie;
import java.util.List;

public interface MovieServiceInterface {

    Movie getById(Integer id);
    void save(Movie movie);
    void delete(Movie movie);
    List<Movie> getAll();
    void update(Movie movieToUpdate, Movie movie);

}
