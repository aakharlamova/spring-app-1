package ru.kharlamova.mySpringApp.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kharlamova.mySpringApp.model.Movie;
import ru.kharlamova.mySpringApp.repository.MovieRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class MovieService implements MovieServiceInterface {

    @Autowired
    MovieRepository movieRepository;

    @Override
    public Movie getById(Integer id) {
        log.info("IN MovieService getById {}", id);
        return movieRepository.findById(id).orElse(null);
    }

    @Override
    public void save(Movie movie) {
        log.info("IN MovieService save {}", movie);
        movieRepository.save(movie);
    }

    @Override
    public void delete(Movie movie) {
        log.info("IN MovieService delete {}", movie);
        movieRepository.delete(movie);
    }

    @Override
    public List<Movie> getAll() {
        log.info("IN MovieService getAll");
        return movieRepository.findAll();
    }

    @Override
    public void update(Movie movieToUpdate, Movie movie) {
        log.info("IN MovieService update {}", movieToUpdate.getId());
        movieToUpdate.setGenre(movie.getGenre());
        movieToUpdate.setTitle(movie.getTitle());
        movieToUpdate.setYear(movie.getYear());
        movieToUpdate.setWatched(movie.isWatched());
        movieRepository.save(movieToUpdate);
    }

}
