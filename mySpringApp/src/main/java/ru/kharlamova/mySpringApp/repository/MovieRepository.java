package ru.kharlamova.mySpringApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kharlamova.mySpringApp.model.Movie;

public interface MovieRepository extends JpaRepository<Movie, Integer> {

}
